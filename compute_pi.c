#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "odd_numbers_struct.h"
#include "compute_pi.h"
#include "generate_odd_numbers.h"
#include "partial_sum.h"

extern void *
generate_odd_numbers(
    void *
);

extern void *
partial_sum(
    void *
);

long double sum;

oddnum * threads;

void * thread_display(
    int,
    long unsigned int,
    char *
);

static void free_resources(
    const int, void *
);

void compute_pi(
    int g_length,
    int num_threads
) {
    threads = malloc(
	num_threads * sizeof( oddnum )
    );
    if( threads == NULL ) abort( );

    params p;
    p.g_length       = g_length;
    p.num_threads    = num_threads;
    p.threads        = threads;

    thread_display(
	syscall(
	    SYS_gettid
	),		   
	pthread_self( ),
	"main thread for process"
    );
    
    int * res = ( int * )generate_odd_numbers( &p );
    if( res == NULL ) {
        fprintf(
            stderr,
            "array of terms failed to build.\n"
        );
        return;
    }
    
    sum = 0.;
    oddnum * thread = threads;
    const int size = num_threads;
    
    for( int i = 0; i < size; ++i ) {
        partial_sum( thread );
	if( i < size ) {
	    sum += thread -> partial_sum;
	    thread += 1;	    
	}
    }
    fprintf(
	stderr,
        "\n\tcompute_pi result is: %Lf.\n",
	sum
    );
    
    free_resources( size, threads );
}

void * thread_display(
    int systid,
    long unsigned int arg,
    char * caption
) {
    fprintf(
	stderr,
	"\t\t"
	"\033[33m"	
        "Thread identifiers for %s:\n"
	"\033[0m",
	caption
    );
    fprintf(
	stderr,
	"\t\t\t"
	"\033[33m"	
        "TID:                      %d\n"
	"\033[0m",
	systid
    );    
    fprintf(
	stderr,
	"\t\t\t"
	"\033[33m"	
        "Thread opaque ID:         %ld\n"
	"\033[0m",
	arg
    );
    return NULL;
}

void free_resources(
    const int size, void * threads
) {
    int index = 0;
    while( index < size ) {
        oddnum * thread = \
	    ( oddnum * )
	    threads;
	thread += index;
	free( thread -> terms );
	++index;
    }
    free( threads );
}
