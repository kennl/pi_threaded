#ifndef _COMPUTE_PI_H_
#define _COMPUTE_PI_H_

#include "odd_numbers_struct.h"

typedef struct {
    int      g_length;
    int      num_threads;
    oddnum * threads;
} params;

void * thread_display(
    int,
    long unsigned int,
    char *
);

#endif
