#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "generate_odd_numbers.h"
#include "compute_pi.h"
#include "odd_numbers_struct.h"

#define EXIT_RES_FAIL -1
#define EXIT_RES_SUCCESS 0

static pthread_mutex_t mutex = \
    PTHREAD_MUTEX_INITIALIZER;

static int g_length;

static int fail = EXIT_RES_FAIL;
static void * exit_res_fail = &fail;

static int success = EXIT_RES_SUCCESS;
static void * exit_res_success = &success;

static int pos = 0;      // mutex locked when in use by a thread

extern void * thread_display(
    int,
    long unsigned int,
    char *
);                       // compute_pi.c has this code

void * runner( void * );

void * generate_odd_numbers(
    void * parameters
) {
    params * p = \
	( params * )
        parameters;      // compute_pi.h has this struct
    const int num_threads = p -> num_threads;
    g_length = p -> g_length;
    float seq_length = ( float )g_length;
    seq_length /= ( float )num_threads;
    seq_length = ceil( seq_length );
    pthread_t      thread_array [ num_threads ];
    pthread_attr_t attr_array   [ num_threads ];

    for( int i = 0; i < num_threads; ++i ) {
	pthread_attr_init( &attr_array[ i ] );	
    }
    int index = 0;
    while( index < num_threads ) {
	oddnum * thread = \
	    ( oddnum * )
	    ( p -> threads );
	thread += index;
	thread -> num_terms = ( int )seq_length;	
	int res = pthread_create(
	    &thread_array[ index ],
	    &attr_array[ index ],
	    runner,
	    thread
	);	
	if( res == EXIT_RES_SUCCESS ) {
	    p -> threads -> opaque_index = \
	        thread_array[ index ];
	} else {
	    p -> num_threads = index;
	    return NULL;
	}
	index += 1;
    }
    index = 0;
    while( index < num_threads ) {
	oddnum * thread = \
	    ( oddnum * )
	    ( p -> threads );
	thread += index;
	pthread_join(
	    thread_array[ index ],
	    NULL
	);
        thread_display(
		thread -> tid,
		thread_array[ index ],
		"runner thread"
	    );	
	pthread_attr_destroy(
	    &attr_array[ index ]    
	);
        index += 1;
    }
    return &g_length;
}

void * runner(
    void * t
) {
    oddnum * thread = \
        ( oddnum * ) t;
    thread -> tid = \
      	syscall(
	    SYS_gettid
	);
    int seq_length = thread -> num_terms;
    long double *
    seq = malloc(
	      seq_length * sizeof(
		  long double
	  ) );
    if( seq == NULL ) {
        abort( );
    }
    for( int i = 0; i < seq_length; ++i ) {
        pthread_mutex_lock( &mutex );
        // the shared resource here, requiring a lock, is int pos	
	if( pos == 0 ) {
	    seq[ i ] = -4.L;
	} else if( pos < g_length ) {
	    seq[ i ] = ( long double )pos;
	} else if( pos ) {
	    seq[ i ] = 0.L;
	} else {
	    return exit_res_fail;	  
	}
	pos += 1;
        pthread_mutex_unlock( &mutex );
    }
    thread -> terms = seq;
    int switchval = 0;
    for( int i = 0; i < seq_length; ++i ) {
        if( seq[ i ] == 0. ) {
	    continue;
	} else if( seq[ i ] < 0. ) {
	    seq[ i ] *= -1.L;
	    continue;
	} else if( fmod(
	    ( double )seq[ i ], 2.
	) == 0. ) {
	    switchval = 1;
	} else if( fmod(
	    ( double )seq[ i ], 2.
	) != 0. ) {
	    switchval = -1;
	} else {
	    return exit_res_fail;
	}
	seq[ i ] *= 2.L;
	seq[ i ] += 1.L;
	seq[ i ] = switchval / seq[ i ];
	seq[ i ] *= 4.L;
    }
    return exit_res_success;
}
