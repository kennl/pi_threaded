pi : main.o compute_pi.o \
     generate_odd_numbers.o \
     partial_sum.o
	cc -o pi compute_pi.o \
	generate_odd_numbers.o \
	partial_sum.o main.o \
	-g -lm -lrt -pthread \
	-fno-pie -no-pie -Wall

partial_sum.o : partial_sum.c \
     generate_odd_numbers.h \
     partial_sum.h \
     compute_pi.h \
     odd_numbers_struct.h
	cc -c partial_sum.c -g -Wall

generate_odd_numbers.o : \
     generate_odd_numbers.c \
     generate_odd_numbers.h \
     odd_numbers_struct.h
	cc -c generate_odd_\
	numbers.c -g -Wall

compute_pi.o : compute_pi.c \
     compute_pi.h \
     generate_odd_numbers.h \
     partial_sum.h \
     odd_numbers_struct.h
	cc -c compute_pi.c -g -Wall

main.o : main.c \
     compute_pi.h \
     generate_odd_numbers.h \
     partial_sum.h \
     odd_numbers_struct.h
	cc -c main.c -g -Wall

clean :
	rm pi compute_pi.o \
	generate_odd_numbers.o \
	partial_sum.o main.o \
