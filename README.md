# compute pi, multithreaded

## Getting Started

A Makefile is provided to assist in building the 'pi' executable on Linux, gcc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0, by executing a terminal command to GNU's native 'make' utility:  "make".  Note:  pthread.h and math.h are among the dependencies.  Note also:  malloc is known to be threadsafe; if pi computes successfully, you demonstrate that malloc is indeed threadsafe to the threadcount.

## Details

Run the program to use posix runner threads to generate and store 200 million terms of a series across 7,000 (dynamically allocated) arrays of long doubles.  Runners rejoin the main thread of the process when complete.  The main thread sums up the values for all 200 million terms to provide an approximate value:  3.141593.  This is actually done quite briskly by an old laptop executing the following terminal commmand to 'pi':  "./pi 200000000 7000".  The executable 'pi' is built from these source files.  Try other arguments for more or fewer terms, or more or fewer arrays.  Two billion terms is possibly too many for some.  In fact, the series that is generated and computed is the Taylor series for arctan( 1 ), an alternating series:

1. The first term is 4 (equal to 4 times 1/1).
2. The next term is -1.33... (equal to -4 times 1/3).
3. The rest of the terms are equal to 4 times 1/j for j = 5, -7, 9, ...

The series converges, at a leisurely pace.
