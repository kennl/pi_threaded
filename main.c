#include <stdlib.h>
#include <stdio.h>

#include "odd_numbers_struct.h"
#include "compute_pi.h"
#include "generate_odd_numbers.h"
#include "partial_sum.h"

extern void compute_pi(
    int, int
);

int main(
    int argc,
    char ** argv
) {
    if( argc != 3 ) {
        fprintf(
            stderr,
            "Command syntax is:  pi "
	    "<integer value for "
	    "g_length>  <integer "
	    "value num_threads>\n"
        );
        return EXIT_FAILURE;
    }
    int g_length    = atoi( argv[1] );
    int num_threads = atoi( argv[2] );    
    fprintf(
	stderr,
	"The integer value for "
	"'g_length' is: %d\n",
	g_length
    );    
    fprintf(
	stderr,
	"The integer value for "
	"'num_threads' is: %d\n\n",
	num_threads
    );

    compute_pi(
        g_length,
	num_threads
    );
    
    return 0;
}
