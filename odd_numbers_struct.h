#ifndef _ODD_NUMBERS_STRUCT_H_
#define _ODD_NUMBERS_STRUCT_H_
typedef struct {
    int               tid;
    long unsigned int opaque_index;
    int               num_terms;
    long double *     terms;
    long double       partial_sum;
} oddnum;
#endif
