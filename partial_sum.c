#include <stdlib.h>

#include "odd_numbers_struct.h"
#include "compute_pi.h"
#include "generate_odd_numbers.h"
#include "partial_sum.h"

static oddnum *      thread;
static int           size;
static long double * term;

void * partial_sum (
    void * t
) {
    thread = (oddnum *) t;
    size = thread -> num_terms;
    term = thread -> terms;
    thread -> partial_sum = 0.;
    for( int i = 0; i < size; ++i )
        thread -> partial_sum += term[ i ];
    return NULL;
}
